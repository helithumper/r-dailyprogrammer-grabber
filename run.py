from handler import grab_and_post

def main():
    # Since the request takes no data, we can pass None for the context
    print(grab_and_post(None, None))

if __name__ == "__main__":
    main()
