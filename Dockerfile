FROM python:3.8-buster

RUN apt-get update && apt-get upgrade -y

RUN curl -sL https://deb.nodesource.com/setup_13.x | bash -
RUN apt-get install nodejs -y

COPY requirements.txt /tmp/

RUN pip3 install -r /tmp/requirements.txt

RUN useradd --create-home builduser
WORKDIR /home/builduser

COPY package.json ./
RUN npm install

RUN npm install -g serverless

COPY . .

USER builduser
