#!python3
import os
import sys
import praw
from github import Github
import gitlab
import requests

issue_disclaimer = "**THIS WAS WRITTEN BY A BOT** _beep boop_"
latest_n_posts = 20

TARGET_SUBREDDIT = 'dailyprogrammer'

class DuplicateSubmissionException(Exception):
    pass

def extract_submission_title(submission_title):
    """
    Extracts Dailyprogrammer submission's title
    Args:
        submission_title: Reddit Submission's Title

    Returns:
        extracted title from post. Example: "N Queens Validator"

    """
    if len(submission_title.split(']')) < 3:
        raise Exception("Invalid submission title")

    title = submission_title.split(']')[2].strip()

    if title is None:
        raise Exception("Submission has empty title")

    return title

def extract_submission_difficulty(submission_title):
    """
    Extracts Dailyprogrammer submission's difficulty
    Args:
        submission_title: Reddit Submission's Title

    Returns:
        extracted difficulty from post. Example: Easy
    """
    if len(submission_title.split(']')) < 3:
        raise Exception("Invalid submission title")

    difficulty = submission_title.split('[')[2].split(']')[0]

    if difficulty is None:
        raise Exception("Submission has empty title")

    return difficulty

def extract_submission_number(submission_title):
    """
    Extracts Dailyprogrammer submission's difficulty
    Args:
        submission_title: Reddit Submission's Title

    Returns:
        extracted difficulty from post. Example: 371
    """
    if len(submission_title.split(']')) < 3:
        raise Exception("Invalid submission title")

    number = submission_title.split('#')[1].split(' ')[0]

    if number is None:
        raise Exception("Submission has no number")

    return number

def create_github_issue(submission):
    """
    Create a GitHub Issue for a dailyprogrammer submission
    Args:
        submission: Reddit Post containing a problem

    Returns:
        None

    Raises:
        DuplicateSubmissionException if a duplicate problem exists

    """
    print(f"Attempting Issue Creation for {submission.title}")
    access_token = os.getenv('GITHUB_ACCESS_TOKEN') or ""
    g = Github(access_token)
    repo_name = os.getenv('GITHUB_REPO_NAME') or ""
    repo = g.get_repo(repo_name)
    issues = repo.get_issues(state='all')

    # Ignore submission if it is a duplicate of an existing issue
    submission_number = extract_submission_number(submission.title)
    for issue in issues:
        current_number = extract_submission_number(issue.title)
        if submission_number == current_number:
            raise DuplicateSubmissionException

    difficulty = extract_submission_difficulty(submission.title)
    title = extract_submission_title(submission.title)

    label = repo.get_label(difficulty)

    repo.create_issue(
        title=submission.title,
        body=f"# {title}\n{issue_disclaimer}\n\n**Original Link:** ["
             f"{submission.url}]"
             f"({submission.url})\n ## Post Body\n{submission.selftext}",
        labels=[label]
    )


def create_gitlab_issue(submission):
    """
    Creates a GitHub Issue for a dailyprogrammer submission
    Args:
        submission: Reddit Post containing a problem

    Returns:
        None

    Raises:
        DuplicateSubmissionException if problem is a duplicate of an existing
        one.

    """
    print(f"Attempting issue creation for {submission.title}")
    auth_token = os.getenv('GITLAB_TOKEN')
    repo_name = os.getenv('GITLAB_REPO')
    gl = gitlab.Gitlab('https://gitlab.com', private_token=auth_token)
    project = gl.projects.get(repo_name)
    if not project:
        raise Exception(f"Gitlab repository {repo_name} not found")

    print(f"Found Project {project.name}")

    new_post_number = extract_submission_number(submission.title)

    # Ignore submission if it is a duplicate of an existing issue
    for issue in project.issues.list():
        issue_challenge_number = extract_submission_number(issue.title)
        if new_post_number == issue_challenge_number:
            raise DuplicateSubmissionException

    title = extract_submission_title(submission.title)
    difficulty = extract_submission_difficulty(submission.title)
    new_issue = project.issues.create({
        'title': submission.title,
        'description': f"# {title}\n{issue_disclaimer}\n\n**Original Link:** "
                       f"[{submission.url}]({submission.url})\n ## Post Body\
                       n{submission.selftext}"
    })


    try:
        new_issue.labels.append(difficulty)
    # Specify the exception type
    except:
        print(f"Could not find difficult label for {difficulty}")
    new_issue.save()


def grab_and_post(event, context):
    reddit_id = os.getenv('REDDIT_CLIENT_ID') or ""
    reddit_secret = os.getenv('REDDIT_CLIENT_SECRET') or ""
    reddit_user_agent = os.getenv('REDDIT_USER_AGENT') or ""

    reddit = praw.Reddit(client_id=reddit_id,
                         client_secret=reddit_secret,
                         user_agent=reddit_user_agent)

    # Since there is no built-in mechanism for checking login status,
    # try grabbing a random subreddit and if it fails then we are unable
    # to login.
    try:
        random_subreddit = reddit.random_subreddit().display_name
        print(f"Successfully Logged in, Random Subreddit: {random_subreddit}")
    except requests.exceptions.ResponseException as e:
        raise Exception(f"Could not login to Reddit")

    reddit_dp = reddit.subreddit(TARGET_SUBREDDIT)
    posts = list(reddit_dp.new(limit=latest_n_posts))

    # We want these in the order of latest to oldest, however
    # we are given them in the opposite order.
    posts.reverse()

    for post in posts:
        print(f"Grabbed Post {post.title}")

        try:
            if os.getenv('GITHUB_ACCESS_TOKEN') and os.getenv('GITHUB_REPO_NAME'):
                create_github_issue(post)
            elif os.getenv('GITLAB_TOKEN') and os.getenv('GITLAB_REPO'):
                create_gitlab_issue(post)
            else:
                print("Github or Gitlab tokens missing. Exiting.")
        except DuplicateSubmissionException:
            print("Submission already exists")


if __name__ == "__main__":
    grab_and_post(None, None)
