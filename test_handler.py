import handler
import praw
import contextlib
import pytest


@contextlib.contextmanager
def does_not_raise():
    yield


@pytest.mark.parametrize("title,expected,expectation", [
    ("asdf", "", pytest.raises(Exception)),
    ("", "", pytest.raises(Exception)),
    ("[2018-12-31] Challenge #371 [Easy] N queens validator", "N queens "
                                                              "validator",
     does_not_raise())
])
def test_extract_submission_title(title, expected, expectation):
    with expectation:
        result = handler.extract_submission_title(title)
        assert result == expected


@pytest.mark.parametrize("title,expected,expectation", [
    ("asdf", "", pytest.raises(Exception)),
    ("", "", pytest.raises(Exception)),
    ("[2018-12-31] Challenge #371 [Easy] N queens validator", "Easy",
     does_not_raise())
])
def test_extract_submission_difficulty(title, expected, expectation):
    with expectation:
        result = handler.extract_submission_difficulty(title)
        assert result == expected

@pytest.mark.parametrize("title,expected,expectation", [
    ("asdf", "", pytest.raises(Exception)),
    ("", "", pytest.raises(Exception)),
    ("[2018-12-31] Challenge #371 [Easy] N queens validator", "371",
     does_not_raise())
])
def test_extract_submission_number(title, expected, expectation):
    with expectation:
        result = handler.extract_submission_number(title)
        assert result == expected

